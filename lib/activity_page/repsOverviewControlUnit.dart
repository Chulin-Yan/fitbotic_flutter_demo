import 'package:flutter/material.dart';

class RepsOverviewControlUnit extends StatelessWidget {
  final double _width;
  final double _height;
  final Function _function;
  final int _index;
  RepsOverviewControlUnit(
    this._width,
    this._height,
    this._function,
    this._index,
  );

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: _width,
      height: _height,
      child: Container(
      // color: _index%2 == 0 ? Colors.red[100] : Colors.yellow[100],
        child: GestureDetector(onTap: () {
          print('control unit $_index is touched');
          _function(_index);
        }),
      ),
    );
  }
}
