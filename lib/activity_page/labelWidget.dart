import 'package:flutter/material.dart';
import 'package:journey_page/globalValues.dart';

class LabelWidget extends StatelessWidget {
  final int _numberToBeShown;
  final double _totalWidth;
  final double _totalHeight;

  LabelWidget(
    this._numberToBeShown,
    this._totalWidth,
    this._totalHeight,
  );

  @override
  Widget build(BuildContext context) {
    return Container(
      //color: Colors.green,
      child: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            // text display box
            Container(
              decoration: BoxDecoration(
                color: GlobalValues.PRIMARY_COLOR,
                borderRadius: BorderRadius.all(
                  Radius.circular(20.0),
                ),
              ),
              child: Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: 0.01 * _totalWidth,
                  vertical: 0.005 * _totalHeight,
                ),
                child: Text(
                  '$_numberToBeShown Reps',
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            // triangle
            SizedBox(
              height: 0.005 * _totalHeight,
              width: 0.02 * _totalWidth,
              child: Container(
                // color: Colors.green,
                child: CustomPaint(
                  painter: Triangle(),
                  // ),
                ),
              ),
            ),
            // concentric circles
            CircleAvatar(
               radius: 0.011 * _totalHeight,
                backgroundColor: GlobalValues.PRIMARY_COLOR,
              child: CircleAvatar(
                radius: 0.01 * _totalHeight,
                backgroundColor: Colors.white,
                child: CircleAvatar(
                  radius: 0.006 * _totalHeight,
                  backgroundColor: GlobalValues.PRIMARY_COLOR,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class Triangle extends CustomPainter {
  Paint painter;
  Triangle() {
    painter = Paint();
    painter.color = GlobalValues.PRIMARY_COLOR;
    painter.style = PaintingStyle.fill;
    painter.strokeWidth = 20.0;
  }
  @override
  void paint(Canvas canvas, Size size) {
    // print(size.height.toString());
    // print(size.width.toString());
    Path path = Path();
    path.moveTo(
      0,
      0,
    );
    path.lineTo(
      size.width,
      0,
    );
    path.lineTo(
      5 * size.width / 10,
      size.height * 0.8,
    );
    path.close();
    canvas.drawPath(path, painter);
   
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}
