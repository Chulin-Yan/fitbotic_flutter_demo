import 'package:flutter/material.dart';
import 'package:journey_page/globalValues.dart';

class ActivityUpperPart extends StatefulWidget {
  final double _totalWidth;
  final double _totalHeight;
  final List<int> _reps;

  ActivityUpperPart(
    this._totalWidth,
    this._totalHeight,
    this._reps,
  );
  @override
  _ActivityUpperPartState createState() => _ActivityUpperPartState();
}

class _ActivityUpperPartState extends State<ActivityUpperPart> {
  
  String _totalWorkouts;
  String _distance;
  String _steps;
  int _sharedValue = 0;
  Color _activeColor = Colors.white;
  Color _inactiveColor = Colors.white70;
  List<Color> _colorList = [];
  List<FontWeight> _fontWeightList = [];

  _ActivityUpperPartState() {
    _colorList.add(_activeColor);
    _colorList.add(_inactiveColor);
    _colorList.add(_inactiveColor);
    _fontWeightList.add(FontWeight.bold);
    _fontWeightList.add(FontWeight.normal);
    _fontWeightList.add(FontWeight.normal);
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              GlobalValues.SECONDARY_COLOR,
              GlobalValues.PRIMARY_COLOR,
            ]),
      ),
      child: Column(
        children: [
          // daily, weekly, monthly reps tab
          Container(
            padding: EdgeInsets.symmetric(
              horizontal: 0.05 * widget._totalWidth,
              vertical: 0.02 * widget._totalHeight,
            ),
            child: Row(
              children: [
                GestureDetector(
                  onTap: () {
                    if (_sharedValue != 0) {
                      _colorList[0] = _activeColor;
                      _colorList[1] = _inactiveColor;
                      _colorList[2] = _inactiveColor;
                      _fontWeightList[0] = FontWeight.bold;
                      _fontWeightList[1] = FontWeight.normal;
                      _fontWeightList[2] = FontWeight.normal;
                      setState(() {
                        _sharedValue = 0;
                      });
                    }
                  },
                  child: Column(
                    children: [
                      Text(
                        'Today',
                        style: TextStyle(
                          fontSize: 14,
                          color: _colorList[0],
                          fontWeight: _fontWeightList[0],
                        ),
                      ),
                      Container(
                        height: 0.005 * widget._totalHeight,
                      ),
                      Container(
                        child: CircleAvatar(
                          backgroundColor: _sharedValue == 0
                              ? Colors.white
                              : Colors.transparent,
                          radius: 3.0,
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  width: 0.05 * widget._totalWidth,
                ),
                GestureDetector(
                  onTap: () {
                    if (_sharedValue != 1) {
                      _colorList[1] = _activeColor;
                      _colorList[0] = _inactiveColor;
                      _colorList[2] = _inactiveColor;
                      _fontWeightList[1] = FontWeight.bold;
                      _fontWeightList[0] = FontWeight.normal;
                      _fontWeightList[2] = FontWeight.normal;
                      setState(() {
                        _sharedValue = 1;
                      });
                    }
                  },
                  child: Column(
                    children: [
                      Text(
                        'This Week',
                        style: TextStyle(
                          fontSize: 14,
                          color: _colorList[1],
                          fontWeight: _fontWeightList[1],
                        ),
                      ),
                      Container(
                        height: 0.005 * widget._totalHeight,
                      ),
                      Container(
                        child: CircleAvatar(
                          backgroundColor: _sharedValue == 1
                              ? Colors.white
                              : Colors.transparent,
                          radius: 3.0,
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  width: 0.05 * widget._totalWidth,
                ),
                GestureDetector(
                  onTap: () {
                    if (_sharedValue != 2) {
                      _colorList[2] = _activeColor;
                      _colorList[1] = _inactiveColor;
                      _colorList[0] = _inactiveColor;
                      _fontWeightList[2] = FontWeight.bold;
                      _fontWeightList[1] = FontWeight.normal;
                      _fontWeightList[0] = FontWeight.normal;
                      setState(() {
                        _sharedValue = 2;
                      });
                    }
                  },
                  child: Column(
                    children: [
                      Text(
                        'This Month',
                        style: TextStyle(
                          fontSize: 14,
                          color: _colorList[2],
                          fontWeight: _fontWeightList[2],
                        ),
                      ),
                      Container(
                        height: 0.005 * widget._totalHeight,
                      ),
                      Container(
                        child: CircleAvatar(
                          backgroundColor: _sharedValue == 2
                              ? Colors.white
                              : Colors.transparent,
                          radius: 3.0,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          // reps, total workouts info boxes
          Container(
            padding: EdgeInsets.symmetric(
              horizontal: 0.05 * widget._totalWidth,
              vertical: 0.02 * widget._totalHeight,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                InfoBox(
                  0.4 * widget._totalWidth,
                  0.1 * widget._totalHeight,
                  'Reps',
                  widget._reps[_sharedValue].toString(),
                  'lib/asset/Icon/jump-rope.png',
                ),
                InfoBox(
                  0.4 * widget._totalWidth,
                  0.1 * widget._totalHeight,
                  'Total Workouts',
                  'N/A',
                  'lib/asset/Icon/skipping-rope.png',
                ),
              ],
            ),
          ),
          // distance, steps info boxes
          Container(
            padding: EdgeInsets.symmetric(
              horizontal: 0.05 * widget._totalWidth,
              vertical: 0.02 * widget._totalHeight,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                InfoBox(
                  0.4 * widget._totalWidth,
                  0.1 * widget._totalHeight,
                  'Distance',
                  'N/A',
                  'lib/asset/Icon/distance.png',
                ),
                InfoBox(
                  0.4 * widget._totalWidth,
                  0.1 * widget._totalHeight,
                  'Steps',
                  'N/A',
                  'lib/asset/Icon/footsteps.png',
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class InfoBox extends StatelessWidget {
  final double _width;
  final double _height;
  final String _text;
  final String _value;
  final String _imageName;

  InfoBox(
    this._width,
    this._height,
    this._text,
    this._value,
    this._imageName,
  );
  @override
  Widget build(BuildContext context) {
    return Container(
      height: _height,
      width: _width,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(20.0),
        ),
        color: Colors.white,
      ),
      child: Container(
        padding: EdgeInsets.symmetric(
          horizontal: 0.1 * _width,
          vertical: 0.1 * _height,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  _text,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 14,
                  ),
                ),
                Text(
                  _value,
                  style: TextStyle(
                    color: GlobalValues.PRIMARY_COLOR,
                    fontSize: 26,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
            Image.asset(_imageName),
          ],
        ),
      ),
    );
  }
}
