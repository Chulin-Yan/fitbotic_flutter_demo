class NotificationItem {
  final String icon;
  final String title;
  final String content;
  bool ticked = false;

  NotificationItem(this.icon, this.title, this.content,);

  void changeTickStatus(){
    ticked = true;
  }
}