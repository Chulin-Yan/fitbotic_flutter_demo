import 'package:flutter/material.dart';
import 'package:journey_page/globalValues.dart';
import 'package:journey_page/notifications_page/notificationItem.dart';
import 'package:journey_page/profile_page/profilePageAppBar.dart';

class NotificationsView extends StatefulWidget {
  final _totalWidth;
  final _totalHeight;

  static int unreadNotifications = 0;
  NotificationsView(
    this._totalWidth,
    this._totalHeight,
  );

  @override
  _NotificationsViewState createState() => _NotificationsViewState();
}

class _NotificationsViewState extends State<NotificationsView> {
  //List<NotificationItem> notificationItemList = [];

  Widget createItem(NotificationItem item) {
    String imageName = '';
    Color color =
        item.ticked == true ? Colors.white : GlobalValues.SECONDARY_COLOR;
    Color textColor = item.ticked == true ? Colors.black : Colors.white;
    switch (item.icon) {
      case 'tick':
        imageName = 'lib/asset/Icon/tick@2x.png';
        break;
      default:
        imageName = 'lib/asset/Icon/fibotic.png';
        break;
    }

    return GestureDetector(
      onTap: () {
        if (item.ticked == false) {
          ProfilePageAppBar.unreadNotifications.value--;
          item.ticked = true;
        }
        setState(() {});
      },
      child: Container(
        height: 0.12 * widget._totalHeight,
        color: color,
        alignment: Alignment.center,
        child: ListTile(
          leading: SizedBox(
            width: 0.07 * widget._totalHeight,
            height: 0.07 * widget._totalHeight,
            child: Image.asset(
              imageName,
              fit: BoxFit.fill,
            ),
          ),
          title: Text(
            item.title,
            style: TextStyle(
              color: textColor,
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
          ),
          subtitle: Text(
            item.content,
            style: TextStyle(
              color: textColor,
              fontSize: 13,
            ),
          ),
        ),
      ),
    );
  }

// should be used to retrieve notifications.
// currently is used to add a new notification
  Future<void> _fetchItems() {
    ProfilePageAppBar.notificationItemList.add(
      new NotificationItem(
        'fibotic',
        'WELCOME TO FITBOTIC!',
        'You can now use all free features of the Fitbotic app!',
      ),
    );
    ProfilePageAppBar.unreadNotifications.value++;
    setState(() {});
  }

  @override
  initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Container(
          height: 0.04 * widget._totalHeight,
          width: widget._totalWidth,
          alignment: Alignment(
            -0.3,
            0.5,
          ),
          child: Text(
            "NOTIFICATIONS",
            style: TextStyle(
              color: Colors.black,
            ),
          ),
        ),
        bottom: PreferredSize(
          preferredSize: Size(
            0.8 * widget._totalWidth,
            0.05 * widget._totalHeight,
          ),
          child: Expanded(
            child: GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: Container(
                alignment: Alignment.center,
                child: Text('Reserved for ads.'),
                color: Colors.white,
              ),
            ),
          ),
        ),
      ),
      body: RefreshIndicator(
        onRefresh: () async {
          await Future.delayed(Duration(seconds: 1),);
          _fetchItems();
        },
        child: ListView(
          children: ProfilePageAppBar.notificationItemList.map((e) {
            return createItem(e);
          }).toList(),
        ),
      ),
    );
  }
}
