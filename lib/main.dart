import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:journey_page/activity_page/activity.dart';
import 'package:journey_page/journey_page/journeyPageAppBar.dart';
import 'package:journey_page/profile_page/profilePageAppBar.dart';
import 'package:journey_page/journey_page/blockListView.dart';
import 'package:journey_page/globalValues.dart';
import 'package:journey_page/profile_page/profilePageBody.dart';
import 'package:journey_page/userInfo.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // primary 0xFF0089FF, secondary 0xFF00C4FF
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        buttonBarTheme: ButtonBarThemeData(
          alignment: MainAxisAlignment.center,
        ),
      ),
      home: MyHomePage(title: 'Journey Page Demo'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

// add some test
// another try
// third try

class _MyHomePageState extends State<MyHomePage> {
  List<Dot> _totalDots = [];
  List<String> _bottomNavBarItemImage;
  MediaQueryData _mediaQuery;
  double _bodyHeight;
  double _bodyWidth;
  int _pageIndex = 0;
  int _accumulatedReps = 0;
  DateTime _currentDateTime = new DateTime(
    2020,
    1,
    1,
  );
  @override
  void initState() {
    super.initState();
    resetBottomNavBarItem();

    // get all dots from the json file
    loadAsset().then((value) {
      setState(() {
        _totalDots = userInfoFromJson(value).dots;
        bool _reachedCurrent = false;
        for (int i = 0; i < _totalDots.length; i++) {
          _totalDots[i].sequenceNum = i;
          _totalDots[i].isDummy = false;
          if (_totalDots[i].status == Status.CURRENT_LOCK) {
            _reachedCurrent = true;
            _currentDateTime = _totalDots[i].day;
            // _currentLockPosition = i;
          }
          if (!_reachedCurrent && _totalDots[i].reps != null) {
            _totalDots[i].finishedReps = _totalDots[i].reps;
            _accumulatedReps += _totalDots[i].reps;
          } else {
            _totalDots[i].finishedReps = 0;
          }
        }
      });
    });
  }

// Transform a json file into a json string
  Future<String> loadAsset() async {
    return await rootBundle.loadString('lib/asset/jsons/journeyMap.json');
  }

// set icons in the bottom nav bar based on which icon have been clicked
  void resetBottomNavBarItem() {
    _bottomNavBarItemImage = [
      _pageIndex == 0
          ? 'lib/asset/Icon/medal_focus.png'
          : 'lib/asset/Icon/medal.png',
      _pageIndex == 1
          ? 'lib/asset/Icon/dollarsign_focus.png'
          : 'lib/asset/Icon/rewards.png',
      _pageIndex == 2
          ? 'lib/asset/Icon/trophy_focus.png'
          : 'lib/asset/Icon/trophy.png',
      _pageIndex == 3
          ? 'lib/asset/Icon/profile_focus.png'
          : 'lib/asset/Icon/profile.png',
      _pageIndex == 4
          ? 'lib/asset/Icon/exercise_focus@2x.png'
          : 'lib/asset/Icon/exercise@2x.png',
    ];
  }

// only journey and profile page have been implemented
  AppBar _getAppBar(int index) {
    AppBar result;
    switch (_pageIndex) {
      case 0:
        result = JourneyPageAppBar(
          _bodyWidth,
          _bodyHeight,
          () {
            setState(() {});
          },
        ).getAppBar();
        break;
      case 3:
        result = new ProfilePageAppBar(
          _bodyWidth,
          _bodyHeight,
          context,
        ).getAppBar();
        break;
      default:
        return AppBar();
    }
    return result;
  }

  @override
  Widget build(BuildContext context) {
    _mediaQuery = MediaQuery.of(context);
    _bodyHeight = _mediaQuery.size.height;
    _bodyWidth = _mediaQuery.size.width;
    List<Widget> _segments = [
      // Structure of the body part the journey map page
      Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              GlobalValues.SECONDARY_COLOR,
              GlobalValues.PRIMARY_COLOR,
            ],
          ),
        ),
        child: Column(
          children: [
            // Space to show the journey map
            Container(
              child: Container(
                height: 0.718 * _bodyHeight,
                child: BlockListView(
                  _bodyWidth,
                  _bodyHeight,
                  _totalDots,
                ),
              ),
            )
          ],
        ),
      ),

      // The activity page
      Container(
        child: Activity(
          _totalDots,
          _currentDateTime,
        ),
      ),
    ];

    return _totalDots.length == 0
        ? Scaffold(
            body: Container(
              alignment: Alignment.center,
              child: CupertinoActivityIndicator(
                radius: 40,
              ),
            ),
          )
        : SafeArea(
            child: Scaffold(
              // implementation of body
              // only journey and profile page have been implemented
              body: _pageIndex == 0
                  ? _segments[JourneyPageAppBar.sharedValue]
                  : _pageIndex == 3
                      ? ProfilePageBody(
                          _bodyWidth,
                          _bodyHeight,
                          _accumulatedReps,
                          'Fitbotic-AabZxv',
                        )
                      : Container(),
              // Implementation of the appBar

              appBar: PreferredSize(
                preferredSize: Size(
                  _bodyWidth,
                  0.1 * _bodyHeight,
                ),
                child: _getAppBar(_pageIndex),
              ),

              // Implementation of the bottom nav bar
              // Should be moved to an individual file
              bottomNavigationBar: Container(
                height: _mediaQuery.size.height * 0.08,
                child: Stack(
                  overflow: Overflow.visible,
                  children: [
                    Positioned(
                      top: _mediaQuery.size.height * 0.007,
                      child: Container(
                        height: _mediaQuery.size.height * 0.08,
                        width: _mediaQuery.size.width,
                        color: Colors.white,
                        child: Container(
                          padding: EdgeInsets.fromLTRB(
                              _bodyWidth * 0.02, 0, _bodyWidth * 0.02, 0),
                          color: Colors.white,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              GestureDetector(
                                onTap: () {
                                  setState(() {
                                    _pageIndex = 0;
                                    resetBottomNavBarItem();
                                    // _bottomNavBarItemImage[0] =
                                    //     'lib/asset/Icon/medal_focus.png';
                                  });
                                },
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Image.asset(_bottomNavBarItemImage[0]),
                                    Text('journey'),
                                  ],
                                ),
                              ),
                              GestureDetector(
                                onTap: () {
                                  setState(() {
                                    _pageIndex = 1;
                                    resetBottomNavBarItem();
                                    // _bottomNavBarItemImage[1] =
                                    //     'lib/asset/Icon/dollarsign_focus.png';
                                  });
                                },
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Image.asset(_bottomNavBarItemImage[1]),
                                    Text('rewards'),
                                  ],
                                ),
                              ),
                              Column(),
                              GestureDetector(
                                onTap: () {
                                  setState(() {
                                    _pageIndex = 2;
                                    resetBottomNavBarItem();
                                    // _bottomNavBarItemImage[2] =
                                    //     'lib/asset/Icon/trophy_focus.png';
                                  });
                                },
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Image.asset(_bottomNavBarItemImage[2]),
                                    Text('challenges'),
                                  ],
                                ),
                              ),
                              GestureDetector(
                                onTap: () {
                                  _pageIndex = 3;
                                  resetBottomNavBarItem();
                                  setState(() {
                                    // _bottomNavBarItemImage[3] =
                                    //     'lib/asset/Icon/profile_focus.png';
                                  });
                                },
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Image.asset(_bottomNavBarItemImage[3]),
                                    Text('profile'),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      left: 0.405 * _bodyWidth,
                      top: -0.03 * _bodyHeight,
                      child: Container(
                        width: 0.09 * _bodyHeight,
                        height: 0.09 * _bodyHeight,
                        decoration: BoxDecoration(
                          //  color: Colors.white,
                          borderRadius: BorderRadius.all(
                            Radius.circular(90.0),
                          ),
                        ),
                        child: RawMaterialButton(
                          elevation: 0.0,
                          disabledElevation: 0.0,
                          focusElevation: 0.0,
                          highlightElevation: 0.0,
                          hoverElevation: 0.0,
                          onPressed: () {
                            setState(() {
                              _pageIndex = 4;
                              resetBottomNavBarItem();
                            });
                          },
                          fillColor: Colors.white,
                          padding: EdgeInsets.all(0.006 * _bodyHeight),
                          shape: CircleBorder(),
                          child: Image.asset(_bottomNavBarItemImage[4]),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
  }
}
