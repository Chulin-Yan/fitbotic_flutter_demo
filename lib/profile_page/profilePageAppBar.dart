import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:journey_page/notifications_page/notificationItem.dart';
import 'package:journey_page/notifications_page/notificationsView.dart';

class ProfilePageAppBar {
  final _totalHeight;
  final _totalWidth;
  BuildContext context;
  static ValueNotifier<int> unreadNotifications;
  static ValueNotifier<int> incompleteSettings;
  static List<NotificationItem> notificationItemList = [];
  ProfilePageAppBar(
    this._totalWidth,
    this._totalHeight,
    this.context,
  );

  void _getList() {
    notificationItemList = [
      NotificationItem(
        'tick',
        'DAILY GOAL COMPLETED!',
        'You completed your Daily goal. Only 2 left to reach your weekly goal!',
      ),
    ];
  }

  _toNotifications(BuildContext context) {
    Navigator.push(
      context,
      CupertinoPageRoute(
        builder: (context) {
          return NotificationsView(
            _totalWidth,
            _totalHeight,
          );
        },
      ),
    );
  }

  Widget getAppBar() {
    _getList();
    unreadNotifications = ValueNotifier<int>(notificationItemList.length);
    incompleteSettings = ValueNotifier<int>(5);
    return AppBar(
      backgroundColor: Colors.white,
      // page title
      title: Container(
        margin: EdgeInsets.only(
          top: 0.01 * _totalHeight,
        ),
        height: 0.03 * _totalHeight,
        width: _totalWidth,
        alignment: Alignment.center,
        child: Text(
          "PROFILE",
          style: TextStyle(
            color: Colors.black,
            fontSize: 20,
          ),
        ),
      ),
      // alert indicator
      leading: Container(
        margin: EdgeInsets.only(
          top: 0.01 * _totalHeight,
        ),
        child: GestureDetector(
          onTap: () {
            _toNotifications(context);
          },
          child: Stack(
            children: [
              Align(
                alignment: Alignment.center,
                child: Container(
                  width: 0.05 * _totalWidth,
                  height: 0.05 * _totalWidth,
                  child: Image.asset(
                    'lib/asset/Icon/alert.png',
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              Align(
                alignment: Alignment(
                  0.4,
                  -0.6,
                ),
                child: ValueListenableBuilder(
                  builder: (BuildContext context, value, Widget child) {
                    return CircleAvatar(
                      backgroundColor: Colors.red,
                      radius: 0.017 * _totalWidth,
                      child: Text(
                        '${value.toString()}',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 10,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    );
                  },
                  valueListenable: unreadNotifications,
                ),
              ),
            ],
          ),
        ),
      ),
      // settings indicator
      actions: [
        Container(
          width: 0.15 * _totalWidth,
          height: 0.15 * _totalWidth,
          margin: EdgeInsets.only(
            top: 0.01 * _totalHeight,
          ),
          child: Stack(
            children: [
              Container(
                child: Align(
                  alignment: Alignment.center,
                  child: Container(
                    padding: EdgeInsets.symmetric(
                      horizontal: 0.03 * _totalWidth,
                    ),
                    child: GestureDetector(
                      child: Container(
                        width: 0.05 * _totalWidth,
                        height: 0.05 * _totalWidth,
                        child: Image.asset(
                          'lib/asset/Icon/setting.png',
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                // color: Colors.blue,
                child: Align(
                  alignment: Alignment(
                    0.4,
                    -0.6,
                  ),
                  child: ValueListenableBuilder(
                    builder: (BuildContext context, value, Widget child) {
                      return CircleAvatar(
                        backgroundColor: Colors.red,
                        radius: 0.017 * _totalWidth,
                        child: Text(
                          '${value.toString()}',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      );
                    },
                    valueListenable: incompleteSettings,
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
      // reserved for ads
      bottom: PreferredSize(
        preferredSize: Size(
          0.8 * _totalWidth,
          0.015 * _totalHeight,
        ),
        child: Expanded(
          child: Container(
            alignment: Alignment.center,
            child: Text('Reserved for ads.'),
            //        color: Colors.green,
          ),
        ),
      ),
    );
  }
}
