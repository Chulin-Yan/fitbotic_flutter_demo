import 'package:flutter/material.dart';

class CircularProgress extends StatefulWidget {
  final int _numerator;
  final int _denominator;
  final double _diameter;
  final Color _valueColor;
  final String _text;

  CircularProgress(
    this._numerator,
    this._denominator,
    this._diameter,
    this._valueColor,
    this._text,
  );

  @override
  _CircularProgressState createState() => _CircularProgressState();
}

class _CircularProgressState extends State<CircularProgress>
    with SingleTickerProviderStateMixin {
  double _value;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _value = widget._denominator == 0.0
        ? 0
        : widget._numerator / widget._denominator * 1.0;
    return Column(
      children: [
        Container(
          width: widget._diameter,
          height: widget._diameter,
          child: Stack(
            children: [
              // circular progress indicator
              Container(
                width: widget._diameter,
                height: widget._diameter,
                child: RotatedBox(
                  quarterTurns: 2,
                  child: TweenAnimationBuilder<double>(
                    tween: Tween<double>(
                      begin: 0.0,
                      end: _value,
                    ),
                    duration: const Duration(
                      milliseconds: 1000,
                    ),
                    builder: (context, value, _) => CircularProgressIndicator(
                      strokeWidth: 6.0,
                      value: value,
                      backgroundColor: Colors.grey[300],
                      valueColor: AlwaysStoppedAnimation(
                        widget._valueColor,
                      ),
                    ),
                  ),
                ),
              ),

              // fraction in the middle
              Container(
                alignment: Alignment.center,
                child: Text(
                  '${widget._numerator}/${widget._denominator}',
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ],
          ),
        ),
        Container(
          height: 0.1 * widget._diameter,
        ),
        Container(
          child: Text(
            widget._text,
            style: TextStyle(
              fontSize: 16,
            ),
          ),
        ),
      ],
    );
  }
}
