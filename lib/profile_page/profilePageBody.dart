import 'package:flutter/material.dart';
import 'package:journey_page/globalValues.dart';
import 'package:journey_page/profile_page/circularProgress.dart';

class ProfilePageBody extends StatelessWidget {
  final double _totalWidth;
  final double _totalHeight;
  final int _accumulatedReps;
  final String _userId;

  ProfilePageBody(
    this._totalWidth,
    this._totalHeight,
    this._accumulatedReps,
    this._userId,
  );

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          // ads
          Container(
            height: 0.05 * _totalHeight,
            padding: EdgeInsets.symmetric(
              horizontal: 0.02 * _totalWidth,
              vertical: 0.01 * _totalHeight,
            ),
            color: GlobalValues.PRIMARY_COLOR,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'UNLOCK 10X MORE REWARDS',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Icon(
                  Icons.arrow_forward,
                  color: Colors.white,
                ),
              ],
            ),
          ),
          // img and greetings
          Container(
            //        color: Colors.green,
            width: 0.9 * _totalWidth,
            height: 0.14 * _totalHeight,
            child: Row(children: [
              Container(
                width: 0.12 * _totalHeight,
                height: 0.12 * _totalHeight,
                child: Image.asset(
                  'lib/asset/Icon/profile@2x.png',
                  fit: BoxFit.fill,
                ),
              ),
              SizedBox(
                width: 0.03 * _totalWidth,
              ),
              Container(
                width: 0.6 * _totalWidth,
                height: 0.15 * _totalHeight,
                child: Container(
                  padding: EdgeInsets.symmetric(
                    vertical: 0.01 * _totalHeight,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        //  color: Colors.red,
                        child: Text(
                          'HELLO, USERNAME!',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Container(
                        // color: Colors.yellow,
                        child: Text(
                          'Awesome! You have completed this week\s goal. Keep it up!',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 14,
                          ),
                        ),
                      ),
                      Container(
                        width: 0.41 * _totalWidth,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          color: Color(0xFFE5A539),
                          borderRadius: BorderRadius.all(
                            Radius.circular(5.0),
                          ),
                        ),
                        child: Container(
                          padding: EdgeInsets.symmetric(
                            horizontal: 0.01 * _totalWidth,
                            vertical: 0.005 * _totalHeight,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                'Member:',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 15,
                                ),
                              ),
                              SizedBox(
                                width: 0.01 * _totalWidth,
                              ),
                              Text(
                                'BRONZE',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              SizedBox(
                                width: 0.03 * _totalWidth,
                              ),
                              Icon(
                                Icons.arrow_forward,
                                size: 0.045 * _totalWidth,
                                color: Colors.white,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ]),
          ),
          // spacing
          Container(
            height: 0.01 * _totalHeight,
          ),
          // progress
          Container(
            height: 0.19 * _totalHeight,
            margin: EdgeInsets.symmetric(
              horizontal: 0.05 * _totalWidth,
            ),
            decoration: BoxDecoration(
              border: Border.all(
                color: Colors.grey[300],
              ),
              borderRadius: BorderRadius.all(
                Radius.circular(30.0),
              ),
            ),
            child: Column(
              children: [
                Container(
                  height: 0.005 * _totalHeight,
                ),
                Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: 0.05 * _totalWidth,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Current Progress',
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Image.asset('lib/asset/Icon/edit-curves.png'),
                    ],
                  ),
                ),
                Container(
                  height: 0.005 * _totalHeight,
                ),
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      CircularProgress(
                        0,
                        3,
                        0.2 * _totalWidth,
                        Color(0xFF1CBD73),
                        'Daily Goal',
                      ),
                      CircularProgress(
                        1,
                        4,
                        0.2 * _totalWidth,
                        Color(0xFFFFD35D),
                        'Weekly Goal',
                      ),
                      CircularProgress(
                        1,
                        3,
                        0.2 * _totalWidth,
                        Color(0xFF1789FC),
                        'Current Tier',
                      ),
                    ],
                  ),
                ),
                Container(
                  height: 0.01 * _totalHeight,
                ),
              ],
            ),
          ),
          // total reps and workouts
          Container(
            child: Row(
              children: [
                Container(
                  margin: EdgeInsets.fromLTRB(
                    0.045 * _totalWidth,
                    0.01 * _totalHeight,
                    0.015 * _totalWidth,
                    0.01 * _totalHeight,
                  ),
                  width: 0.44 * _totalWidth,
                  height: 0.1 * _totalHeight,
                  decoration: BoxDecoration(
                    //  color: Colors.green,
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                    borderRadius: BorderRadius.all(
                      Radius.circular(15.0),
                    ),
                  ),
                  child: Container(
                    padding: EdgeInsets.symmetric(
                      horizontal: 0.02 * _totalWidth,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Total Reps',
                              style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Image.asset('lib/asset/Icon/jump-rope.png'),
                          ],
                        ),
                        Text(
                          _accumulatedReps.toString(),
                          style: TextStyle(
                            fontSize: 30,
                            fontWeight: FontWeight.bold,
                            color: GlobalValues.PRIMARY_COLOR,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(
                    0.015 * _totalWidth,
                    0.01 * _totalHeight,
                    0.045 * _totalWidth,
                    0.01 * _totalHeight,
                  ),
                  width: 0.44 * _totalWidth,
                  height: 0.1 * _totalHeight,
                  decoration: BoxDecoration(
                    // color: Colors.green,
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                    borderRadius: BorderRadius.all(
                      Radius.circular(15.0),
                    ),
                  ),
                  child: Container(
                    padding: EdgeInsets.symmetric(
                      horizontal: 0.02 * _totalWidth,
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Total Workouts',
                              style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Image.asset('lib/asset/Icon/skipping-rope.png'),
                          ],
                        ),
                        Text(
                          'N/A',
                          style: TextStyle(
                            fontSize: 30,
                            fontWeight: FontWeight.bold,
                            color: GlobalValues.PRIMARY_COLOR,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          // invite friends
          Container(
            width: 0.9 * _totalWidth,
            height: 0.2 * _totalHeight,
            margin: EdgeInsets.symmetric(
              horizontal: 0.05 * _totalWidth,
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(
                Radius.circular(15.0),
              ),
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  GlobalValues.SECONDARY_COLOR,
                  GlobalValues.PRIMARY_COLOR,
                ],
              ),
            ),
            child: Stack(
              overflow: Overflow.visible,
              children: [
                Container(
                  width: 0.9 * _totalWidth,
                  height: 0.2 * _totalHeight,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        padding: EdgeInsets.symmetric(
                          vertical: 0.01 * _totalHeight,
                          horizontal: 0.02 * _totalWidth,
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              padding: EdgeInsets.only(
                                bottom: 0.005 * _totalHeight,
                              ),
                              child: Text(
                                'Invite a Friend',
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            Container(
                              width: 0.47 * _totalWidth,
                              child: Text(
                                'You and a friend will receive \$5 each when you both successfully subscribe to a paid subscription. Successful referrals during your trial period will be pending until you subscribe.',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 14,
                                ),
                                textAlign: TextAlign.justify,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(
                          horizontal: 0.05 * _totalWidth,
                        ),
                        height: 0.13 * _totalHeight,
                        width: 0.2 * _totalWidth,
                        child: Image.asset(
                          'lib/asset/Icon/fitbotic_invite@2x.png',
                          fit: BoxFit.fill,
                        ),
                      ),
                    ],
                  ),
                ),
                Align(
                  alignment: Alignment(
                    0.85,
                    1.3,
                  ),
                  child: Container(
                    width: 0.4 * _totalWidth,
                    height: 0.05 * _totalHeight,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(
                          Radius.circular(40.0),
                        ),
                        boxShadow: [
                          BoxShadow(blurRadius: 2.5, spreadRadius: 0.0),
                        ]),
                    child: Container(
                      padding: EdgeInsets.symmetric(
                        horizontal: 0.025 * _totalWidth,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            _userId,
                            style: TextStyle(
                              color: GlobalValues.PRIMARY_COLOR,
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Image.asset('lib/asset/Icon/share.png'),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
