import 'package:flutter/material.dart';
import 'dart:async';
import 'package:journey_page/journey_page/journeyBlock.dart';
import 'package:journey_page/userInfo.dart';
import 'journeyPageInfoBoard.dart';

class BlockListView extends StatefulWidget {
  final double _bodyWidth;
  final double _bodyHeight;
  List<Dot> allDots = [];

  BlockListView(
    this._bodyWidth,
    this._bodyHeight,
    this.allDots,
  );
  @override
  _BlockListViewState createState() => _BlockListViewState();
}

class _BlockListViewState extends State<BlockListView> {
  Future response;
  bool _hasReachedCurrentLock = false;
  bool _moreCheck = true;
  int _changeOpacityPosition = 0;
  List<List<Dot>> availableDotList = [];
  List<String> _bodyTextList = [];
  List<List<String>> partitionedBodyText = [];

  void getDotList() {
    int index = 0;
    while (true) {
      if (index + 12 < widget.allDots.length) {
        // print(availableDotList.length.toString());
        availableDotList.add(
          widget.allDots.sublist(
            index,
            index + 12,
          ),
        );
        //  print(availableDotList.length.toString());
      } else {
        availableDotList.add(
          widget.allDots.sublist(
            index,
            widget.allDots.length,
          ),
        );
        break;
      }
      index += 12;
    }
    //print('total dotList:');
    // print(availableDotList.length.toString());
  }

  @override
  void initState() {
    super.initState();
    // find out from where should the path and dots on it be semi-transparent
    for (int i = 0; i < widget.allDots.length; i++) {
      if (widget.allDots[i].body != null) {
        _bodyTextList.add(widget.allDots[i].body);
      }
      if (widget.allDots[i].status == Status.CURRENT_LOCK) {
        _hasReachedCurrentLock = true;
      }
      if (_hasReachedCurrentLock &&
          widget.allDots[i].dotType != Type.DAILY &&
          _moreCheck) {
        _changeOpacityPosition = i;
        _moreCheck = false;
      }
    }
    // add dummy dots to make allDots.length % 12 == 0
    int _originalLength = widget.allDots.length;
    int count = 0;
    // print('total dots without dummy: $_originalLength');
    if (widget.allDots.length % 12 != 0) {
      for (int i = 0; i < 12 - _originalLength % 12; i++, count++) {
        widget.allDots.add(
          new Dot(
            status: Status.FAIL,
            sequenceNum: _originalLength + i,
            dotType: Type.DAILY,
            isDummy: true,
          ),
        );
      }
    }
    // text list partition
    for (var i = 0; i < _bodyTextList.length; i += 2) {
      var end = (i + 2 < _bodyTextList.length ? i + 2 : _bodyTextList.length);
      partitionedBodyText.add(
        _bodyTextList.sublist(i, end),
      );
    }
    getDotList();
  }

  @override
  Widget build(BuildContext context) {
    if (availableDotList.length != 0) {
      return ListView.builder(itemBuilder: (context, index) {
        if (index == 0) {
          return JourneyPageInfoBoard(
            MediaQuery.of(context),
            20.0,
          );
        }
        if (index - 1 < availableDotList.length) {
          return JourneyBlock(
            widget._bodyWidth,
            widget._bodyHeight,
            availableDotList[index - 1],
            index - 1,
            _changeOpacityPosition,
            index - 1 > partitionedBodyText.length
                ? null
                : partitionedBodyText[index],
          );
        }
        return null;
      });
    } else {
      return Container();
    }
  }
}
