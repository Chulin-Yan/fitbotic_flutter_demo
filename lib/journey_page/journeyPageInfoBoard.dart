import 'package:flutter/material.dart';

class JourneyPageInfoBoard extends StatelessWidget {
  final MediaQueryData _mediaQuery;
  final double _cornerRadius;
  double _bodyWidth;
  double _bodyHeight;
  JourneyPageInfoBoard(
    this._mediaQuery,
    this._cornerRadius,
  ) {
    _bodyWidth = _mediaQuery.size.width;
    _bodyHeight = _mediaQuery.size.height;
  }
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: _mediaQuery.size.height * 0.05,
          width: double.infinity,
          child: Column(
            children: [

              // space reserved for one-line ads
              Container(
                width: _mediaQuery.size.width,
                height: _mediaQuery.size.height * 0.05,
                alignment: Alignment.center,
                color: Color(0xFF0089FF),
                child: Container(
                  color: Color(0x0A87BB),
                  child: Text(
                    'Place reserved for ads or notifications',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 14,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),

        //outer most rectangle
        Container(
          padding: EdgeInsets.all(_mediaQuery.size.height * 0.01),
          height: _mediaQuery.size.height * 0.20,
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(
                Radius.circular(_cornerRadius),
              ),
              color: Color(0xFF0A87BB),
            ),
            child: Column(
              children: [

                // Title of the info board
                Container(
                  height: _mediaQuery.size.height * 0.05,
                  child: Center(
                    child: Text(
                      'TODAY\'S GOAL',
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                      ),
                    ),
                  ),
                ),

                // Content of the info board
                Container(
                  height: _mediaQuery.size.height * 0.13,
                  padding: EdgeInsets.fromLTRB(
                    _bodyWidth * 0.02,
                    _bodyHeight * 0.01,
                    _bodyWidth * 0.02,
                    _bodyHeight * 0.01,
                  ),
                  child: Stack(
                    children: [
                      Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[

                            // left inner rectangle
                            Container(
                              height: _mediaQuery.size.height * 0.12,
                              width: _mediaQuery.size.width * 0.43,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(_cornerRadius),
                                  bottomLeft: Radius.circular(_cornerRadius),
                                ),
                                color: Color(0xFF53ABCF),
                              ),
                              child: Center(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      'COMPLETED',
                                      style: TextStyle(
                                        fontSize: 15.0,
                                        fontWeight: FontWeight.normal,
                                        color: Colors.white70,
                                      ),
                                    ),
                                    Text(
                                      '0 OF 50',
                                      style: TextStyle(
                                        fontSize: 20.0,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white,
                                      ),
                                    ),
                                    Text(
                                      'REPS',
                                      style: TextStyle(
                                        fontSize: 20.0,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(
                              width: _mediaQuery.size.width * 0.01,
                            ),
                            // right inner rectangle
                            Container(
                              height: _mediaQuery.size.height * 0.115,
                              width: _mediaQuery.size.width * 0.43,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(_cornerRadius),
                                  bottomRight: Radius.circular(_cornerRadius),
                                ),
                                color: Color(0xFF53ABCF),
                              ),
                              child: Center(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      'EARN',
                                      style: TextStyle(
                                        fontSize: 15.0,
                                        fontWeight: FontWeight.normal,
                                        color: Colors.white70,
                                      ),
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Image.asset(
                                          'lib/asset/Icon/coin.png',
                                          height: 0.03 * _bodyHeight,
                                          width: 0.03 * _bodyHeight,
                                          fit: BoxFit.fill,
                                        ),
                                        SizedBox(
                                          width: 0.01 * _bodyWidth,
                                        ),
                                        Text(
                                          '10',
                                          style: TextStyle(
                                            fontSize: 20.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white,
                                          ),
                                        ),
                                      ],
                                    ),
                                    Text(
                                      'COINS',
                                      style: TextStyle(
                                        fontSize: 20.0,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ]),
                          
                      // equals sign in the middle
                      Center(
                        child: CircleAvatar(
                          radius: _mediaQuery.size.width * 0.03,
                          backgroundColor: Theme.of(context).primaryColorDark,
                          child: Text(
                            '=',
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }
}
